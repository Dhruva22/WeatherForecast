package com.mitross.weatherforecast.application.builder;

import com.mitross.weatherforecast.utils.rx.AppRxSchedulers;
import com.mitross.weatherforecast.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {

    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }

}
