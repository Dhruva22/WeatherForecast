package com.mitross.weatherforecast.application.builder;

import android.content.Context;

import com.mitross.weatherforecast.utils.rx.RxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {NetworkModule.class, AppContextModule.class,RxModule.class})
public interface AppComponent {
    Context getAppContext();
    RxSchedulers rxSchedulers();

    //ApiInterface appAnalysisApiService();

}
