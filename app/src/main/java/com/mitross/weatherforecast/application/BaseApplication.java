package com.mitross.weatherforecast.application;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDexApplication;

import com.mitross.weatherforecast.application.builder.AppComponent;
import com.mitross.weatherforecast.application.builder.AppContextModule;
import com.mitross.weatherforecast.utils.StaticUtils;

public class BaseApplication extends MultiDexApplication {

    private static AppComponent appComponent;
    public static boolean isAppWentToBg = true;

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
        StaticUtils.setWindowDimensions(this);
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder().appContextModule(new AppContextModule(this)).build();
    }

    public static AppComponent getNetComponent() {
        return appComponent;
    }


    /**
     * Method to check Current Internet connectivity.
     * @return  return true if connection available else returns false.
     */
    @SuppressWarnings("unused")
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            return false;
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();

    }




}
