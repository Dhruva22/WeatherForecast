package com.mitross.weatherforecast.screens.home.core;

import com.mitross.weatherforecast.screens.home.HomeActivity;

public class HomeScreenModel {

    private HomeActivity context;

    public HomeScreenModel(HomeActivity mContext) {
        this.context = mContext;
    }

    public HomeActivity getContext() {
        return context;
    }


    void navigateAppListingScreen() {
        context.navigateToAppListingScreen();
    }

    public void navigateToSettingScreen() {

        context.navigateToSettingScreen();

    }
}
