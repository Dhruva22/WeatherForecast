package com.mitross.weatherforecast.screens.home

import com.mitross.weatherforecast.screens.base.BaseActivity
import com.mitross.weatherforecast.screens.home.core.HomeScreenPresenter
import com.mitross.weatherforecast.screens.home.core.HomeScreenView
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    @Inject
    var view: HomeScreenView? = null
    @Inject
    internal var presenter: HomeScreenPresenter? = null


}