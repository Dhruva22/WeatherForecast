package com.mitross.weatherforecast.screens.home.dagger;

import com.mitross.weatherforecast.screens.home.HomeActivity;
import com.mitross.weatherforecast.screens.home.core.HomeScreenModel;
import com.mitross.weatherforecast.screens.home.core.HomeScreenPresenter;
import com.mitross.weatherforecast.screens.home.core.HomeScreenView;

import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

public class HomeScreenModule {

    HomeActivity context;
    public HomeScreenModule(HomeActivity context){
        this.context = context;
    }

    @HomeScreenScope
    @Provides
    HomeScreenPresenter providePresenter(HomeScreenModel model, HomeScreenView splashView) {
        CompositeDisposable subscriptions = new CompositeDisposable();
        return new HomeScreenPresenter(model,splashView,subscriptions);
    }

    @HomeScreenScope
    @Provides
    HomeScreenView provideSplashView(HomeActivity context) {
        return new HomeScreenView(context);
    }

    @HomeScreenScope
    @Provides
    HomeActivity provideContext() {
        return context;
    }


    @HomeScreenScope
    @Provides
    HomeScreenModel provideSplashModel(HomeActivity ctx) {
        return new HomeScreenModel(ctx);
    }
}
