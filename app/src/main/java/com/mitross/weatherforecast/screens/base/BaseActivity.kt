package com.mitross.weatherforecast.screens.base

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.os.Build
import android.content.Intent
import android.support.v4.app.ActivityOptionsCompat
import android.view.View
import com.mitross.weatherforecast.R
import com.mitross.weatherforecast.utils.PopUtils
import java.util.*


open class BaseActivity : AppCompatActivity() {

    lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val localeString = Locale.getDefault().language
        val locale = Locale(localeString)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = locale
        res.updateConfiguration(conf, dm)

    }

    @SuppressWarnings("unused")
    fun navigateToDifferentScreen(nextScreenIntent: Intent) {
        navigateToDifferentScreen(nextScreenIntent, null, "", false, false, 0, 0)

    }

    @SuppressWarnings("unused")
    fun navigateToDifferentScreen(nextScreenIntent: Intent, view: View, sharedElementName: String, finishActivity: Boolean) {
        navigateToDifferentScreen(nextScreenIntent, view, sharedElementName, false, finishActivity, 0, 0)

    }

    @SuppressWarnings("unused")
    fun navigateToDifferentScreen(nextScreenIntent: Intent, view: View, sharedElementName: String) {
        navigateToDifferentScreen(nextScreenIntent, view, sharedElementName, false, false, 0, 0)

    }

    @SuppressWarnings("unused")
    fun navigateToDifferentScreen(nextScreenIntent: Intent, finishActivity: Boolean) {
        navigateToDifferentScreen(nextScreenIntent, null, "", false, finishActivity, 0, 0)

    }

    @SuppressWarnings("unused")
    fun navigateToDifferentScreen(nextScreenIntent: Intent, finishActivity: Boolean, startAnimation: Int, endAnimation: Int) {
        navigateToDifferentScreen(nextScreenIntent, null, "", true, finishActivity, startAnimation, endAnimation)

    }

    /**
     * Common method to navigate in different activity class
     * @param nextScreenIntent object of Intent
     * @param view view which will be animate
     * @param sharedElementName transition name
     * @param isAnimate boolean for screen animation
     * @param finishActivity boolean set true if want to finish current activity
     * @param startAnimation this is start animation use full if isAnimate is true
     * @param endAnimation this is end animation use full if isAnimate is true
     */
    fun navigateToDifferentScreen(nextScreenIntent: Intent, view: View?, sharedElementName: String,
                                  isAnimate: Boolean, finishActivity: Boolean, startAnimation: Int, endAnimation: Int) {

        try {
            if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, sharedElementName)
                startActivity(nextScreenIntent, options.toBundle())
                if (finishActivity) {
                    finish()
                }
            } else {
                startActivity(nextScreenIntent)
                if (isAnimate) {
                    overridePendingTransition(startAnimation, endAnimation)
                }

                if (finishActivity) {
                    finish()
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun finishActivity() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                supportFinishAfterTransition()
            } else {
                finish()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @SuppressWarnings("unused")
    fun onBackPressFromActivity() {
        onBackPressFromActivity(null, false)
    }

    @SuppressWarnings("unused")
    fun onBackPressFromActivity(isHomeScreen: Boolean) {
        onBackPressFromActivity(null, isHomeScreen)
    }

    private fun onBackPressFromActivity(nextScreenIntent: Intent?, isHomeScreen: Boolean) {

        try {
            if (isHomeScreen) {
                PopUtils.showCustomTwoButtonAlertDialog(this@BaseActivity, "", getString(R.string.app_name),
                        getString(R.string.yes), getString(R.string.no), false, { dialog, which ->
                    dialog.dismiss()
                    this@BaseActivity.finishActivity()
                }, { dialog, which -> dialog.dismiss() })
            } else {
                finishActivity()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}