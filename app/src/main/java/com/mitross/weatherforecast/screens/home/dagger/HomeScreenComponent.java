package com.mitross.weatherforecast.screens.home.dagger;

import com.mitross.weatherforecast.application.builder.AppComponent;
import com.mitross.weatherforecast.screens.home.HomeActivity;

import dagger.Component;

@HomeScreenScope
@Component(dependencies = {AppComponent.class},modules = {HomeScreenModule.class} )
public interface HomeScreenComponent {
    void inject(HomeActivity activity);
}
