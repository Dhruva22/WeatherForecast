package com.mitross.weatherforecast.datalayers.retrofit

import android.util.Log
import com.mitross.weatherforecast.BuildConfig

import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitProvider {

    private val loggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
        message -> Log.e("response", message) })
            .setLevel(HttpLoggingInterceptor.Level.BODY)

    private fun getHttpClient(): OkHttpClient? {

        if (okHttpClient == null) {
            okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .build()
        }
        return okHttpClient

    }

    /**
     * Method to return retrofit instance. This method will retrofit retrofit instance with ad Base url.
     * @return instance of ad retrofit.
     */
    private fun getAppRetrofit(): Retrofit {
        return if (appRetrofit != null) {
            appRetrofit as Retrofit
        } else {
            appRetrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .client(getHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            appRetrofit as Retrofit
        }

    }

    fun <S> createAppService(serviceClass: Class<S>): S {
        return getAppRetrofit().create(serviceClass)
    }


    companion object {
        private var appRetrofit: Retrofit? = null
        private var okHttpClient: OkHttpClient? = null
    }

}