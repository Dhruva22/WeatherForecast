package com.mitross.weatherforecast.datalayers.retrofit

import retrofit2.http.POST
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiInterface {

    @GET("/forecast.json")
    fun getForecastData(@Query("appId") Options: String): Call<AdDataResponse>

}