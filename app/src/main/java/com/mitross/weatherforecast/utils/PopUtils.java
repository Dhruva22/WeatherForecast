package com.mitross.weatherforecast.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Window;

import com.mitross.weatherforecast.R;

public class PopUtils {
    /**
     * Method to show alert dialog with two/single button
     * @param context context
     * @param message Message of dialog
     * @param posButtonName Name of positive button
     * @param nagButtonName Name of negative button
     * @param onPositiveButtonClick call back method of positive button
     * @param onNegativeButtonClick call back method of negative butoon
     */
    public static void showCustomTwoButtonAlertDialog(final Context context, String title, String message, String posButtonName,
                                                      String nagButtonName, boolean isOutSideCancelable,
                                                      DialogInterface.OnClickListener onPositiveButtonClick,
                                                      DialogInterface.OnClickListener onNegativeButtonClick) {
        showCustomTwoButtonAlertDialog(context,title,message,posButtonName,nagButtonName,false,isOutSideCancelable,onPositiveButtonClick,onNegativeButtonClick);

    }

    /**
     * Method to show alert dialog with two/single button
     * @param context context
     * @param message Message of dialog
     * @param posButtonName Name of positive button
     * @param nagButtonName Name of negative button
     * @param onPositiveButtonClick call back method of positive button
     * @param onNegativeButtonClick call back method of negative butoon
     */
    @SuppressWarnings("unused")
    private static void showCustomTwoButtonAlertDialog(final Context context, String title, String message, String posButtonName,
                                                       String nagButtonName, boolean changeButtonColor, boolean isOutSideCancelable,
                                                       DialogInterface.OnClickListener onPositiveButtonClick,
                                                       DialogInterface.OnClickListener onNegativeButtonClick) {
        try {
            if(context!=null) {

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context, R.style.DialogTheme);
                }
                builder.setMessage(message);
                if (TextUtils.isEmpty(title)) {
                    builder.setTitle(context.getResources().getString(R.string.app_name));
                }else{
                    builder.setTitle(title);
                }

                if (onPositiveButtonClick != null) {
                    builder.setPositiveButton(posButtonName, onPositiveButtonClick);
                }
                if (onNegativeButtonClick != null) {
                    builder.setNegativeButton(nagButtonName, onNegativeButtonClick);
                }

                builder.setCancelable(true);
                final AlertDialog alert = builder.create();
                alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                if (changeButtonColor) {
                    try {
                        alert.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (isOutSideCancelable){
                    alert.setCanceledOnTouchOutside(true);
                }else{
                    alert.setCanceledOnTouchOutside(false);
                }
                alert.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
