package com.mitross.weatherforecast.utils;

public class StaticData {
    //Add all static data here
    public static int SCREEN_HEIGHT, SCREEN_WIDTH;
    public static final String APIUX_KEY = "68926eb3086640e68e6204101180704";
    public static final String REQUEST_PARAMS_KEY = "key";
    public static final String REQUEST_PARAMS_Q = "q";
    public static final String REQUEST_PARAMS_DT = "dt";
    public static final String REQUEST_PARAMS_DAY = "days";
    public static final String EXTRA_TAG = "tag";
}
