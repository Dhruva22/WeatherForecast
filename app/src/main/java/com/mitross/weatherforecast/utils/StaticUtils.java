package com.mitross.weatherforecast.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.mitross.weatherforecast.application.BaseApplication;

public class StaticUtils {

    /**
     * This method is used set window dimensions
     *
     * @param context context of current screen.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static void setWindowDimensions(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager == null)
            return;
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        StaticData.SCREEN_WIDTH = size.x;
        StaticData.SCREEN_HEIGHT = size.y;

    }
}
